package com.sxf.util;

public class NTConstants {
	/**
	 * 返回错误标志
	 */
	public final static String OUT_FLAG = "out_flag";
	/**
	 * 返回错误信息
	 */
	public final static String OUT_MSG = "out_msg";
}
