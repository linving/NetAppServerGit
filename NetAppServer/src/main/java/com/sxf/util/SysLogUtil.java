package com.sxf.util;

import org.apache.log4j.Logger;

/**
 * 日志公共类
 * 
 * @author phsxf01
 * 
 */
public class SysLogUtil {
	public static Logger log = Logger.getLogger(SysLogUtil.class);

	public static void debug(Object message) {
		log.debug(message);
	}

	public static void debug(Object message, Throwable t) {
		log.debug(message, t);
	}

	public static void info(Object message) {
		log.info(message);
	}

	public static void info(Object message, Throwable t) {
		log.info(message, t);
	}

}
