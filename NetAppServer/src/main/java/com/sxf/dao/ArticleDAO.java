package com.sxf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sxf.domain.content.Article;

@Repository
public interface ArticleDAO extends JpaRepository<Article, Integer>,
		JpaSpecificationExecutor<Article> {

}
