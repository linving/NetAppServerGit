package com.sxf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sxf.domain.content.File;

@Repository
public interface FileDAO extends JpaRepository<File, Integer>,
		JpaSpecificationExecutor<File> {

}
