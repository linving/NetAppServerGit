package com.sxf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sxf.domain.User;

/**
 * @author ThinkPad
 *
 */
@Repository
public interface UserDAO extends JpaRepository<User, Integer>,
		JpaSpecificationExecutor<User> {

}
