package com.sxf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sxf.domain.content.Subject;

@Repository
public interface SubjectDAO extends JpaRepository<Subject, Integer>,
		JpaSpecificationExecutor<Subject> {

}