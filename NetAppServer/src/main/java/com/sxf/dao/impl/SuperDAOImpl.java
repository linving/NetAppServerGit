package com.sxf.dao.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.jdbc.core.JdbcTemplate;

import com.sxf.dao.SuperDAO;
import com.sxf.mybatis.help.ExRowBounds;
import com.sxf.mybatis.help.QueryArrayList;
import com.sxf.mybatis.help.QueryType;
import com.sxf.mybatis.page.Pagination;
import com.sxf.querybean.base.BaseQuery;

public abstract class SuperDAOImpl implements SuperDAO {
	/**
	 * JdbcTemplate
	 */
	@Resource(name = "jdbcTemplate")
	protected JdbcTemplate jt;

	/**
	 * MyBatis Template
	 */
	@Resource(name = "sqlSessionTemplate")
	protected SqlSessionTemplate st;

	@PersistenceContext
	protected EntityManager em;

	
	
	/**
	 * 分页通用写法
	 * 
	 * @param pageStatement
	 *            分页的sql
	 * @param numberStatement
	 *            查询总数的sql
	 * @param query
	 *            查询bean 必须继承BaseQuery
	 * @return
	 */
	public <A> Pagination<A> queryPageTemplate(String pageStatement,
			String numberStatement, BaseQuery query) {
		query.calculate();
		int rowCount = (Integer) st.selectOne(numberStatement, query);
		List<A> list = st.selectList(pageStatement, query);
		Pagination<A> page = new Pagination<A>(query.getCurrentPage(),
				rowCount, query.getPageSize(), list);

		return page;
	}

	/**
	 * 分页通用写法(最多取30条数据)
	 * 
	 * @param queryStatement
	 * @param query
	 * @return
	 */
	public <A> Pagination<A> queryPageTemplate(String queryStatement,
			BaseQuery query) {
		if (query.getLimit() == BaseQuery.NO_ROW_LIMIT
				&& query.getOffset() == BaseQuery.NO_ROW_OFFSET) {
			query.calculate();//
		}
		List<A> list = st.selectList(queryStatement, query, new ExRowBounds(
				query.getOffset(), query.getLimit(), QueryType.OBJECT_NUMBER));
		Pagination<A> page = null;
		if (list != null && list instanceof QueryArrayList) {
			QueryArrayList<A> queryList = (QueryArrayList<A>) list;
			page = new Pagination<A>(query.getCurrentPage(),
					queryList.getRowTotal(), query.getPageSize(), list);
		}

		return page;
	}

	/**
	 * 查询list模板(默认不分页,除非sql语句包含分页)
	 * 
	 * @param queryStatement
	 * @param query
	 * @return
	 */
	public <A> List<A> queryListTemplate(String queryStatement, BaseQuery query) {
		return st.selectList(queryStatement, query,
				new ExRowBounds(query.getOffset(), query.getLimit(),
						QueryType.OBJECT));
	}

	/**
	 * 查询单个对象模板
	 * 
	 * @param queryStatement
	 * @param query
	 * @return
	 */
	public <A> A queryOneTemplate(String queryStatement, BaseQuery query) {
		List<A> list = st.selectList(queryStatement, query);
		if (list != null && !list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 查询条数模板
	 * 
	 * @param queryStatement
	 * @param query
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public long queryNumberTemplate(String queryStatement, BaseQuery query) {
		query.calculate();
		long count = 0;
		List list = st.selectList(queryStatement, query, new ExRowBounds(
				QueryType.NUMBER));
		if (list != null && list instanceof QueryArrayList) {
			count = ((QueryArrayList) list).getRowTotal();
		}
		return count;
	}

	/**
	 * 分页查询对象
	 * 
	 * @param queryStatement
	 * @param query
	 * @return
	 */
	@SuppressWarnings("unused")
	private <A> List<A> queryPageListTemplate(String queryStatement,
			BaseQuery query) {
		return st.selectList(queryStatement, query,
				new ExRowBounds(query.getOffset(), query.getLimit(),
						QueryType.OBJECT));
	}

	public void setJt(JdbcTemplate jt) {
		this.jt = jt;
	}

	public void setSt(SqlSessionTemplate st) {
		this.st = st;
	}

}
