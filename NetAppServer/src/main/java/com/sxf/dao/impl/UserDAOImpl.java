package com.sxf.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.sxf.domain.User;

public class UserDAOImpl extends SuperDAOImpl {

	public void test() {
		// 获得Criteria建立器
		CriteriaBuilder cb = em.getCriteriaBuilder();
		// 建立Criteria查询
		CriteriaQuery<User> query = cb.createQuery(User.class);
		// 创建查询Root
		Root<User> root = query.from(User.class);
		// 创建firstName的查询条件，使用静态元模型
		Predicate firstNameIs = cb.equal(root.get("name"), "John");
		// 指定查询的where条件
		query.where(firstNameIs);
		// 创建查询并获取结果
		TypedQuery<User> q = em.createQuery(query);
		List<User> Users = q.getResultList();
	}
}
