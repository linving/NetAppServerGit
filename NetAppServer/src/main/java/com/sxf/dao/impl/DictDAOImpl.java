package com.sxf.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.sxf.domain.content.Dict;

@Repository
public class DictDAOImpl {
	@PersistenceContext
	private EntityManager em;

	public void save(Dict d) {
		em.persist(d);
	}
}
