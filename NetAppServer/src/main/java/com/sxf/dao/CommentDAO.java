package com.sxf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sxf.domain.content.Comment;

@Repository
public interface CommentDAO extends JpaRepository<Comment, Integer>,
		JpaSpecificationExecutor<Comment> {

}
