package com.sxf.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sxf.dao.DBAnalysisDAO;
import com.sxf.service.DBAnalysisService;

@Service("DBAnalysisService")
public class DBAnalysisServiceImpl implements DBAnalysisService {
	@Resource(name = "DBAnalysisDAO")
	private DBAnalysisDAO dd;

	@Override
	public Map<Integer, Object> getAllTables() {
		return dd.getAllTables();
	}

	@Override
	public Map<Integer, List<String>> getAllProc() {
		return dd.getAllProc();
	}

	public void setDd(DBAnalysisDAO dd) {
		this.dd = dd;
	}
}
