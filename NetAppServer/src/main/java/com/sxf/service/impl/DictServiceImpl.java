package com.sxf.service.impl;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.sxf.dao.impl.DictDAOImpl;
import com.sxf.domain.content.Dict;

@Service
public class DictServiceImpl {
	@Resource
	private DictDAOImpl dd;

	@Transactional
	public void save(Dict d) {
		dd.save(d);
	}
}
