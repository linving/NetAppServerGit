package com.sxf.domain.content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * 主题管理
 * 
 * @author sxf
 *
 */
@Entity
@Table(name = "subject")
@DynamicInsert
@DynamicUpdate
public class Subject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	/**
	 * 主题名称
	 */
	@Column
	private String name;

	/**
	 * 父主题
	 */
	@Column
	private Integer parent;

	@OneToMany(mappedBy = "subjectId", cascade = CascadeType.ALL)
	private List<Article> arts = new ArrayList<Article>();

	public Subject() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Article> getArts() {
		return arts;
	}

	public void setArts(List<Article> arts) {
		this.arts = arts;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}
}
