package com.sxf.domain.content;

import java.io.Serializable;
import java.sql.Types;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.sxf.orm.ObjectJsonType;

/**
 * 字典表
 * 
 * @author shixiafeng
 *
 */
@Entity
@Table(name = "dict")
@DynamicInsert
@DynamicUpdate
@TypeDefs({ @TypeDef(name = "ObjectJsonType", typeClass = ObjectJsonType.class) })
@Cacheable
public class Dict implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 字段类型
	 */
	@Id
	@Column(name = "type")
	private String type;
	/**
	 * 字典值
	 */
	@Id
	@Column(name = "val")
	private String val;

	/**
	 * 多语言
	 */
	@Id
	@Column(name = "language")
	private String language;

	/**
	 * 文本值
	 */
	private String text;
	/**
	 * 描述
	 */
	@Column(name = "comment", length = 500)
	private String comment;

	// @Lob
	@Column
	@Type(type = "ObjectJsonType", parameters = {
			@Parameter(name = ObjectJsonType.CLASS_TYPE, value = "java.util.Map"),
			@Parameter(name = ObjectJsonType.TYPE, value = Types.CLOB + "") })
	private Map<String, Object> attrs;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, Object> attrs) {
		this.attrs = attrs;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((val == null) ? 0 : val.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dict other = (Dict) obj;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (val == null) {
			if (other.val != null)
				return false;
		} else if (!val.equals(other.val))
			return false;
		return true;
	}

}
