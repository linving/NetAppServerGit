package com.sxf.domain.content;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * @author sxf
 *
 */
@Entity
@Table(name = "comment")
@DynamicInsert
@DynamicUpdate
public class Comment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;

	/**
	 * 
	 */
	@Column(name = "article_id")
	private Long articleId;

	/**
	 * 评论者姓名
	 */
	@Column(name = "name")
	private String name;

	/**
	 * 评论者email
	 */
	@Column(name = "email")
	private String email;

	/**
	 * 评论者IP
	 */
	@Column(name = "ip")
	private String ip;

	/**
	 * 评论日期
	 */
	@Column(name = "date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	/**
	 * 评论内容
	 */
	@Lob
	@Column(name = "content")
	private String content;

	@Column(name = "user_id")
	private Integer userId;

	public Comment() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
