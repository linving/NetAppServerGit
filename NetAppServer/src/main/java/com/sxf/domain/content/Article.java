package com.sxf.domain.content;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * @author sxf
 *
 */
@Entity
@Table(name = "article")
@DynamicInsert
@DynamicUpdate
public class Article implements Serializable {
	/**
     *
     */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	/**
	 * 标题
	 */
	@Column(name = "title")
	private String title;

	/**
	 * 创建日期
	 */
	@Column(name = "create_time")
	@Temporal(TemporalType.TIME)
	private Date createTime;

	/**
	 * 文章内容
	 */
	@Lob
	@Column(name = "content")
	private String content;

	/**
	 * 文章状态:发布,草稿.
	 */
	@Column
	private String status;

	/**
	 * 对应的主题
	 */
	private Integer subjectId;

	public Article() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
