package com.sxf.mybatis.dialect;

public class MySQLDialect extends Dialect {
	@Override
	public String getLimitString(String sql, int offset, int limit) {
		StringBuilder sb = new StringBuilder();
		sb.append(sql);
		sb.append(" limit ");
		sb.append(offset);
		sb.append(",");
		sb.append(limit);
		return sb.toString();
	}

	@Override
	public String getLimitStringByHibernate(String sql, boolean hasOffset) {
		return sql + (hasOffset ? " limit ?, ?" : " limit ?");
	}
}
