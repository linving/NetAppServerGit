package com.sxf.mybatis.dialect;

public abstract class Dialect {
	public static enum Type {
		MYSQL, ORACLE
	}

	/**
	 * @param sql
	 * @param skipResults
	 * @param maxResults
	 * @return
	 */
	public abstract String getLimitString(String sql, int skipResults,
			int maxResults);

	/**
	 * 返回的分页语句带有?号
	 * 
	 * @param sql
	 * @param skipResults
	 * @param maxResults
	 * @return
	 */
	public String getLimitStringPrepared(String sql, int skipResults,
			int maxResults) {
		return this.getLimitStringByHibernate(sql, skipResults, maxResults);
	}

	/**
	 * Hibernate 中的limit方法
	 * 
	 * @param query
	 * @param offset
	 * @param limit
	 * @return
	 */
	public String getLimitStringByHibernate(String query, int offset, int limit) {
		return getLimitStringByHibernate(query, (offset > 0));
	}

	/**
	 * Hibernate 中的limit方法
	 * 
	 * @param sql
	 * @param hasOffset
	 * @return
	 */
	public abstract String getLimitStringByHibernate(String sql,
			boolean hasOffset);
}
