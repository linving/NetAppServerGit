package com.sxf.mybatis.page;

import java.io.Serializable;
import java.util.List;

public class Pagination<T> implements Serializable {

	private static final long serialVersionUID = -6579435031214235862L;

	/**
	 * 总记录数
	 */
	private long total;
	/**
	 * 每页记录数
	 */
	private int pageSize = 10;// 每页记录数
	/**
	 * 总页数
	 */
	private long pageTotal;// 总页数
	private int currentPage;// 当前页数
	private boolean next;// 是否能下一页
	private boolean previous;// 是否能上一页
	private boolean available = true;// 是否分页
	private List<T> rows;// 结果集

	public Pagination() {

	}

	public Pagination(Integer currentPage, long total, int pageSize) {
		if (currentPage == null) {
			this.currentPage = 1;
		} else {
			this.currentPage = currentPage;
		}
		this.total = total;
		if (pageSize > 0) {
			this.pageSize = pageSize;
		}
		// 计算总页数
		this.pageTotal = (long) Math.ceil(this.total / (double) this.pageSize);
		// 计算是否能上一页和下一页
		this.next = this.currentPage < this.pageTotal;
		this.previous = this.currentPage > 1;

	}

	public Pagination(Integer currentPage, long total, int pageSize,
			List<T> rows) {
		this(currentPage, total, pageSize);
		this.rows = rows;
	}

	public Pagination<T> pagination() {
		if (currentPage == 0) {
			this.currentPage = 1;
		}
		// 计算总页数
		this.pageTotal = (int) Math.ceil(this.total / (double) this.pageSize);
		// 计算是否能上一页和下一页
		this.next = this.currentPage < this.pageTotal;
		this.previous = this.currentPage > 1;
		return this;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentMinRow() {
		return (currentPage - 1) * pageSize;
	}

	public int getCurrentMaxRow() {
		return currentPage * pageSize - 1;
	}

	public int getNextPage() {
		if (next) {
			return currentPage + 1;
		}
		return currentPage;
	}

	public int getPreviousPage() {
		if (previous) {
			return currentPage - 1;
		}
		return currentPage;
	}

	public boolean isNext() {
		return next;
	}

	public boolean isPrevious() {
		return previous;
	}

	public int nextPage() {
		if (next) {
			currentPage = currentPage + 1;
		}
		return currentPage;
	}

	public int previousPage() {
		if (previous) {
			currentPage = currentPage - 1;
		}
		return currentPage;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	public long getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(long pageTotal) {
		this.pageTotal = pageTotal;
	}
}
