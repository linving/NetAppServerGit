package com.sxf.mybatis.help;

import org.apache.ibatis.session.RowBounds;

/**
 * 扩展Mybatis的RowBounds方法,添加是否支持统计总条数功能
 * 
 * @author SXF
 * 
 */
public class ExRowBounds extends RowBounds {

	private String countSQL;
	private long count;// 记录的总条数 (无分页):用来传递数据的

	private QueryType queryType = QueryType.OBJECT;// 查询类型:1,查询对象;2,查询总数;3,查询对象和总数

	public ExRowBounds() {
		super();
	}

	public ExRowBounds(int offset, int limit) {
		super(offset, limit);
	}

	public ExRowBounds(QueryType queryType) {
		super();
		this.queryType = queryType;
	}

	public ExRowBounds(int offset, int limit, QueryType queryType) {
		super(offset, limit);
		this.queryType = queryType;
	}

	public String getCountSQL() {
		return countSQL;
	}

	public void setCountSQL(String countSQL) {
		this.countSQL = countSQL;
	}

	/**
	 * 记录的总条数 (无分页)
	 * 
	 * @return
	 */
	public long getCount() {
		return count;
	}

	/**
	 * 记录的总条数 (无分页)
	 * 
	 * @param count
	 */
	public void setCount(long count) {
		this.count = count;
	}

	public QueryType getQueryType() {
		return queryType;
	}

	public void setQueryType(QueryType queryType) {
		this.queryType = queryType;
	}
}
