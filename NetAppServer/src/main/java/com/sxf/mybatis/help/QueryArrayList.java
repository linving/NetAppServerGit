package com.sxf.mybatis.help;

import java.util.ArrayList;
import java.util.List;

/**
 * 返回分页结果集和统计总数
 * 
 * @author SXF
 * 
 * @param <E>
 */
public class QueryArrayList<E> extends ArrayList<E> implements QueryList<E> {
	private static final long serialVersionUID = 4884895814220600477L;

	private long rowTotal;// 总记录

	public QueryArrayList() {

	}

	public QueryArrayList(List<E> list) {
		super(list);
	}

	public QueryArrayList(List<E> list, long rowTotal) {
		super(list);
		this.rowTotal = rowTotal;
	}

	public QueryArrayList(long rowTotal) {
		this.rowTotal = rowTotal;
	}

	@Override
	public List<E> getData() {
		return this;
	}

	@Override
	public long getRowTotal() {
		return this.rowTotal;
	}

	@Override
	public boolean hasMoreData() {
		if (rowTotal > 0 || !isEmpty()) {
			return true;
		}
		return false;
	}

	public void setRowTotal(long rowTotal) {
		this.rowTotal = rowTotal;
	}

}
