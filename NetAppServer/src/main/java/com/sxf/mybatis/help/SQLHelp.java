package com.sxf.mybatis.help;

/**
 * @author SXF
 */
public class SQLHelp {
    /**
     * 得到查询总数的sql
     */
    public static String getCountString(String querySelect) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from ( ");
        sb.append(querySelect);
        sb.append(" ) t");
        return sb.toString();
    }

}
