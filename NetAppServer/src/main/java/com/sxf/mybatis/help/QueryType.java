package com.sxf.mybatis.help;

/**
 * 查询类型:<br>
 * OBJECT:查询对象<br>
 * NUMBER:查询条数<br>
 * OBJECT_NUMBER:查询对象和条数
 */
public enum QueryType {
    /**
     * 查询对象
     */
    OBJECT,
    /**
     * 查询条数
     */
    NUMBER,
    /**
     * 查询对象和条数
     */
    OBJECT_NUMBER
}
