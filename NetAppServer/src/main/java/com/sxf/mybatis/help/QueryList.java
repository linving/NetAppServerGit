package com.sxf.mybatis.help;

import java.util.List;

/**
 * 返回分页结果集和统计总数
 * 
 * @author SXF
 * 
 * @param <E>
 */
public interface QueryList<E> extends List<E> {

	/**
	 * 
	 * 当前页数据
	 * 
	 * @return
	 */
	List<E> getData();

	/**
	 * 
	 * 总行数
	 * 
	 * @return
	 */
	long getRowTotal();

	/**
	 * 
	 * 只分页不统计总行数时true,该值并不真正代表数据库中还有更多的数据，只代表可能还有数据
	 * 
	 * @return
	 */
	boolean hasMoreData();

}