package com.sxf.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import com.sxf.util.NTConstants;

/**
 * ajax返回ajax错误
 * 
 * @author ThinkPad
 *
 */
public class SimpleHandleException extends SimpleMappingExceptionResolver {
	@Override
	protected ModelAndView doResolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		if (handler != null && handler instanceof HandlerMethod) {
			HandlerMethod hand = (HandlerMethod) handler;
			if (hand.getMethodAnnotation(ResponseBody.class) != null) {
				ModelAndView mav = new ModelAndView();
				View view = new FastJsonJsonView();
				mav.setView(view);
				mav.addObject(NTConstants.OUT_FLAG, "1");
				mav.addObject(NTConstants.OUT_MSG, ex.getMessage());
				ex.printStackTrace();
				return mav;
			}
		}
		return super.doResolveException(request, response, handler, ex);
	}
}
