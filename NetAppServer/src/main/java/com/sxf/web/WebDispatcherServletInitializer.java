package com.sxf.web;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import jetbrick.template.web.servlet.JetTemplateServlet;

import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.support.AbstractDispatcherServletInitializer;
import org.springframework.web.util.IntrospectorCleanupListener;
import org.springframework.web.util.Log4jConfigListener;

@Order(3)
public class WebDispatcherServletInitializer extends
		AbstractDispatcherServletInitializer {
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.context.AbstractContextLoaderInitializer#
	 * createRootApplicationContext()
	 */
	@Override
	protected WebApplicationContext createRootApplicationContext() {
		XmlWebApplicationContext ctx = new XmlWebApplicationContext();
		ctx.setConfigLocations(new String[] { "classpath:config/spring/applicationContext.xml" });
		// ctx.refresh(); //不需要refresh
		return ctx;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.support.AbstractDispatcherServletInitializer
	 * #createServletApplicationContext()
	 */
	@Override
	protected WebApplicationContext createServletApplicationContext() {
		XmlWebApplicationContext ctx = new XmlWebApplicationContext();
		ctx.setConfigLocations(new String[] { "classpath*:config/spring/mvc/springmvc-servlet-AllInOne.xml" });
		return ctx;
	}

	@Override
	public void onStartup(ServletContext servletContext)
			throws ServletException {
		super.onStartup(servletContext);
		// Log4jConfigListener
		servletContext.setInitParameter("log4jConfigLocation",
				"classpath:log4j.properties");
		servletContext.setInitParameter("webAppRootKey", "myappfuse.root");
		servletContext.setInitParameter("log4jRefreshInterval", "60000");
		servletContext.addListener(Log4jConfigListener.class);

		servletContext.addListener(RequestContextListener.class);
		servletContext.addListener(IntrospectorCleanupListener.class);

		// OpenSessionInViewFilter
		// OpenSessionInViewFilter hibernateSessionInViewFilter = new
		// OpenSessionInViewFilter();
		// FilterRegistration.Dynamic filterRegistration = servletContext
		// .addFilter("hibernateFilter", hibernateSessionInViewFilter);
		// filterRegistration.addMappingForUrlPatterns(EnumSet.of(
		// DispatcherType.REQUEST, DispatcherType.FORWARD,
		// DispatcherType.INCLUDE), false, "/");

		// jda的sessionView
		OpenEntityManagerInViewFilter entityManagerInViewFilter = new OpenEntityManagerInViewFilter();
		FilterRegistration.Dynamic filterRegistration = servletContext
				.addFilter("entityManagerInViewFilter",
						entityManagerInViewFilter);
		filterRegistration.addMappingForUrlPatterns(EnumSet.of(
				DispatcherType.REQUEST, DispatcherType.FORWARD,
				DispatcherType.INCLUDE), false, new String[] { "/" });

		// JetTemplateServlet
		
		ServletRegistration.Dynamic dynamic = servletContext.addServlet(
				"jetServlet", JetTemplateServlet.class);
		dynamic.setInitParameter("jetbrick-template-config-location", "/WEB-INF/jetbrick-template.properties");
		dynamic.setLoadOnStartup(2);
		dynamic.addMapping("*.jetx");
		
	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter f = new CharacterEncodingFilter();
		f.setEncoding("UTF-8");
		f.setForceEncoding(true);
		// 浏览器不支持put,delete等method,由该filter将/blog?_method=delete转换为标准的http
		// delete方法
		HiddenHttpMethodFilter f2 = new HiddenHttpMethodFilter();
		return new Filter[] { f, f2 };
	}
}
