package com.sxf.web.controller.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rubyeye.xmemcached.MemcachedClient;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * @author phsxf01
 * 
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

	private MemcachedClient memcachedClient;

	// 生成视图之前执行(Action):
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		return super.preHandle(request, response, handler);
	}

	// 生成视图之前执行(controller方法执行后执行)
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler, ModelAndView mav)
			throws Exception {
		/*
		 * Object obj = mav.getModel().get(Constants.LOGIN_USER); if (obj !=
		 * null && obj instanceof UserVO) { UserVO user = (UserVO) obj; String
		 * uuid = UUID.randomUUID().toString(); memcachedClient.set(uuid,
		 * Constants.SESSION_TIME_SECONDS, user); CookieUtils.addCookie(request,
		 * response, CookieUtils.USER_ID, uuid); }
		 */
		super.postHandle(request, response, handler, mav);
	}

	// 最后执行，可用于释放资源
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

}
