package com.sxf.web.controller;

import java.util.Map;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

/**
 * 主类方法
 * 
 * @author phsxf01
 */
public class SuperController extends MultiActionController {

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		// binder.registerCustomEditor(Date.class, new DateConvertEditor());
		// binder.registerCustomEditor(String.class, new
		// StringTrimmerEditor(true));
	}

	/**
	 * 返回ModelAndView
	 * 
	 * @param map
	 * @param viewName
	 * @return
	 */
	public ModelAndView getModelAndView(Map<String, Object> map, String viewName) {
		ModelAndView mav = new ModelAndView(viewName, map);
		return mav;
	}
	
}
