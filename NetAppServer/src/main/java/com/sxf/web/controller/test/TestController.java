package com.sxf.web.controller.test;import javax.servlet.http.HttpServletRequest;import javax.servlet.http.HttpServletResponse;import javax.servlet.http.HttpSession;import org.springframework.jdbc.core.JdbcTemplate;import org.springframework.stereotype.Controller;import org.springframework.ui.ModelMap;import org.springframework.web.bind.annotation.PathVariable;import org.springframework.web.bind.annotation.RequestMapping;import org.springframework.web.bind.annotation.RequestMethod;import org.springframework.web.context.ContextLoader;import org.springframework.web.context.WebApplicationContext;import org.springframework.web.context.request.RequestContextHolder;import org.springframework.web.context.request.ServletRequestAttributes;import org.springframework.web.context.support.WebApplicationContextUtils;import org.springframework.web.servlet.DispatcherServlet;import org.springframework.web.servlet.ModelAndView;import org.springframework.web.servlet.support.RequestContextUtils;import com.sxf.web.controller.SuperController;;

/**
 * 测试Controller
 * 
 * @author SXF
 * 
 */
@Controller
@RequestMapping
public class TestController extends SuperController {
	public TestController() {

	}

	@RequestMapping(value = "/login/{user}", method = RequestMethod.GET)
	public ModelAndView myMethod(HttpServletRequest request,
			HttpServletResponse response, HttpSession session,
			@PathVariable(value = "user") String user, ModelMap modelMap)
			throws Exception {
		// ServletContext context = request.getSession().getServletContext();
		WebApplicationContext wac1 = RequestContextUtils
				.getWebApplicationContext(request);

		WebApplicationContext wac2 = WebApplicationContextUtils
				.getWebApplicationContext(getServletContext(),
						DispatcherServlet.WEB_APPLICATION_CONTEXT_ATTRIBUTE);

		// CotextLoaderListener
		WebApplicationContext wac3 = ContextLoader
				.getCurrentWebApplicationContext();

		WebApplicationContext wac4 = WebApplicationContextUtils
				.getRequiredWebApplicationContext(getServletContext());

		WebApplicationContext wac5 = (WebApplicationContext) getServletContext()
				.getAttribute(
						DispatcherServlet.SERVLET_CONTEXT_PREFIX
								+ getServletContext().getServletContextName());

		System.out.println("wac1:" + wac1);
		System.out.println("wac2:" + wac2);
		System.out.println("wac3:" + wac3);
		System.out.println("wac4:" + wac4);
		System.out.println("wac5:" + wac5);

		System.out.println("wac1 == wac2::" + (wac1 == wac2));
		System.out.println(wac2 == null);
		System.out.println("wac1 == wac3::" + (wac1 == wac3));
		System.out.println("wac1==wac4::" + (wac1 == wac4));
		System.out.println("wac1==wac5::" + (wac1 == wac5));
		System.out.println("wac2==wac3::" + (wac2 == wac3));
		System.out.println("wac2==wac4::" + (wac2 == wac4));
		System.out.println("wac2==wac5::" + (wac2 == wac5));
		System.out.println("wac3==wac4::" + (wac3 == wac4));// true
		System.out.println("wac3==wac5::" + (wac3 == wac5));
		System.out.println("wac4==wac5::" + (wac4 == wac5));

		JdbcTemplate lsb = (JdbcTemplate) wac3.getBean("jdbcTemplate");
		System.out.println("3::LSB:" + lsb);

		modelMap.put("loginUser", user);
		return new ModelAndView("/hello", modelMap);
	}

	@RequestMapping(value = "/welcome", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String registPost() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		System.out.println(request.getContextPath() + " "
				+ request.getServerPort() + " " + request.getLocalAddr() + " "
				+ request.getLocalPort() + " ");
		System.out.println("sdfkdsjfdsjfds");
		return "/welcome";
	}

	

	@RequestMapping(value = "/testvm", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String testVm() {
		@SuppressWarnings("unused")
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();

		// HttpServletResponse response=((servletres) RequestContextHolder
		// .getRequestAttributes()).get();
		return "/test";
	}

}
