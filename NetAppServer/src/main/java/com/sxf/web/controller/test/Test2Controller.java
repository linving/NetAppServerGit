package com.sxf.web.controller.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxf.domain.User;
import com.sxf.domain.content.Dict;
import com.sxf.service.impl.DictServiceImpl;
import com.sxf.service.impl.UserServerImpl;

@Controller
public class Test2Controller {
	@Resource
	private UserServerImpl ud;
	@Resource
	private DictServiceImpl ds;

	@RequestMapping("/t2/test")
	@ResponseBody
	public String test() {
		User u = new User();
		u.setName("Sam");
		u.setEmail("a@3.com");
		u.setBirthday(new Date());
		//ud.save(u);
		System.out.println(ud.getClass());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("test", "3434");
		Dict d = new Dict();
		d.setAttrs(map);
		d.setVal(UUID.randomUUID().toString());
		d.setType(UUID.randomUUID().toString());
		//ds.save(d);
		ud.findBySpecAndPaginate();
		return null;
	}
}